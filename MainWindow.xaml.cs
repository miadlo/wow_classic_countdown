﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WoWClassicCountdown
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var logo = new BitmapImage(); // All of this is used to actually load the logo into memory to reference in the UI
            logo.BeginInit();
            logo.UriSource = new Uri(@"images\logo.png", UriKind.Relative);
            logo.EndInit();

            DispatcherTimer countDown = new DispatcherTimer();  // New timer that is the actual countdown, updates once a second calling the function countDown_Tick
            countDown.Interval = TimeSpan.FromSeconds(1);
            countDown.Tick += countDown_Tick;
            countDown.Start();
        }

        void countDown_Tick(object sender, EventArgs e)
        {
            DateTime release = new DateTime(2019, 8, 26, 14, 00, 00);  // Release date (August 26, 2019 2 PM EST) converted to a DateTime.  Assuming EST since I will use this and other people are EST as well
            TimeSpan remaining = release - DateTime.Now;  // Using TimeSpan to actually calculate how much time is left
            lblTimer.Content = remaining.ToString(@"dd\.hh\:mm\:ss"); // Update label and format the remaining time as Day. Hour. Minute. Seconds, because definitely don't need milliseconds
        }
    }
}
